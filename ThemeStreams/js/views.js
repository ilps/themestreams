DocumentView = Backbone.View.extend({  
	tagName : "div",  
	className: "document well",
	initialize: function() {
		this.model.bind('change', this.render, this);
		this.model.bind('destroy', this.remove, this);
    },
    template: _.template($("#template-document").html()),
	render : function() {
		this.$el.html(this.template(this.model.toJSON()));
		return this;
	}  
});    

FacetView = Backbone.View.extend({  
	tagName : "div",  
	className: "facet",
	initialize: function() {
		this.model.bind('change', this.render, this);
		this.model.bind('destroy', this.remove, this);
		//this.model.bind('change:hits', this.render, this);
    },
    template: _.template($("#template-facet").html()),
    hit_template: _.template($("#template-facet-hit").html()),
	render : function() {
		this.$el.html(this.template(this.model.toJSON()));
		var field = this.model.get("field");
		var target = this.$el.children("#drop-" + field);
		var hits = this.model.get("hits");
		if(hits) _.each(hits[hits["_type"]], _.bind(function(hit) {
			target.append(this.hit_template({"hit": hit, "field": field}));
		}, this));

		return this;
	}  
});
// TODO: Fill drop list in FacetView on query