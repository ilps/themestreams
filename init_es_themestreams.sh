#!/bin/sh


ES_HOST=http://localhost:8004
ES_INDEX=tweets_v2


# Create Index, and Analyzer
curl -XPUT ${ES_HOST}/${ES_INDEX} -d '{
  "settings" : {
    "analysis" : {
      "filter" : {
        "tweet_filter" : {
          "type" : "word_delimiter",
          "type_table": ["# => ALPHA", "@ => ALPHA"]
        }
      },
      "analyzer" : {
        "tweet_analyzer" : {
          "type" : "custom",
          "tokenizer" : "whitespace",
          "filter" : ["lowercase", "tweet_filter"]
        }
      }
    }
  },
  "mappings" : {
    "status" : {
      "properties" : {
        "text" : {
          "type" : "string",
          "analyzer" : "tweet_analyzer"
        }
      }
    }
  }
}'


# Create Default Percolator Named themestreams
curl -XPUT ${ES_HOST}/${ES_INDEX}/.percolator/themestreams -d '{
  "query" : {
    "match_all" : {}
  }
}'

# Set Default ttl for Other Percolators
curl -XPOST ${ES_HOST}/${ES_INDEX}/.percolator/_mapping -d '{
  ".percolator" : {
    "_ttl" : { "enabled" : true, "default" : "1d" }
  }
}'


# Create Twitter River
curl -XPUT ${ES_HOST}/_river/${ES_INDEX}/_meta -d '{
  "type": "twitter",
  "twitter": {
    "filter": {
      "user_lists": "themestreams/politician,themestreams/lobbyist,themestreams/journalist,themestreams/other"
    }
  }
}'

