function drawRadviz(data){
	$('#radviz').empty();
	var words=wordAxis.domain();
	var rect_height=10;
	var rect_width=10;
	var number_of_words = words.length;
	var data_radviz = data;
	var width=$('#radviz').width(),height=$('#radviz').height();
	var margin={top:10,bottom:16,left:(width-height-13)/2,right:(width-height-13)/2};
	var x = d3.scale.linear().range([0+margin.left, width-margin.right]);
	var y = d3.scale.linear().range([0+margin.top, height-margin.bottom]);
	var x_text = d3.scale.linear().range([0, width-40]);
	var y_text = d3.scale.linear().range([2, height]);
	var point_color = d3.scale.linear().domain([1,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0]).range(colorbrewer.RdYlGn[10])
	var angle=Math.PI*2/number_of_words;
	
	var max=getMax(data_radviz,angle);
	x.domain([-max,max]);
	y.domain([max,-max]);
	x_text.domain([-max,max]);
	y_text.domain([max,-max]);
	svg_radviz = d3.select("#radviz").append("svg")
		.attr("class","draw_area")
		.attr("width", width)
    	.attr("height", height)
    	.style("background-color","white");
	
	var graph = svg_radviz.append("g")
		.attr("class","radviz");
	
	var circles = graph.selectAll("circle")
		.data(data_radviz).enter()
		.append("circle")
		.attr("cx",function(d,i){
			var sum=0;
			for(var j=0;j<d.scores.length;j++){
				sum+=d.scores[j]*Math.cos(angle*j);
			}
			sum += jitter(0.05*max);
			return x(sum);
		})
		.attr("cy",function(d,i){
			var sum=0;
			for(var j=0;j<d.scores.length;j++){
				sum+=d.scores[j]*Math.sin(angle*j);
			}
			sum += jitter(0.05*max);
			return y(sum);
		})
		.attr("fill",function(d){
			var media=0;
			var sum=0;
			var numberRelevantWords=0;
			for(var j=0;j<d.scores.length;j++){
				if (d.scores[j]!=0)
				{	
					sum+=d.scores[j];
					numberRelevantWords++;
				}
			}
			if (numberRelevantWords!=0)
				{
					media=sum/numberRelevantWords;
				}
			return point_color(media);
		})
		.attr("r", 1.3);
	
	graph.selectAll("rect")
		.data(words).enter()
		.append("rect")
			.attr("width",rect_width)
			.attr("height",rect_height)
  			.attr("x",function(d,i){
  				return x(max*Math.cos(angle*i));
  			})
			.attr("y",function(d,i){
  				return y(max*(Math.sin(angle*i)));
  			})
  			.attr("fill", function(d,i) { return wordAxis(words[i]); })
  			.attr("transform","translate(" + -rect_width/2 + "," + -rect_height/2 + ")");
	
	graph.selectAll("text")
		.data(words).enter()
		.append("text")
  			.attr("x",function(d,i){
  				return x_text(max*Math.cos(angle*i))
  			})
			.attr("y",function(d,i){
  				return y_text(max*Math.sin(angle*i))
  			})
			.text(function(d){return d;})
//			.attr("text-anchor", "left")
			.attr("font-weight","normal")
			.attr("fill","#000000")				
			.attr("font-family","sans-serif")
			.attr("font-size","10");
	
	dispatch.on("highlightWords.radviz", function(words) {
		d3.select("#radviz").select("svg").selectAll("circle")
			.style("opacity", function(d, i) { 
				var visible = words.length == 0;
				$.each(words, function(index, word) {
					var wordIndex = wordAxis.domain().indexOf(word);
					if (d.scores[wordIndex] > 0) visible = true;
				});
				return visible ? 1 : 0;
			});	
		d3.select("#radviz").select("svg").selectAll("text")
			.style("font-weight", function(d, i) {
				return (words.indexOf(d) != -1) ? "bold" : "normal";
			});	
	});		
	dispatch.on("highlightTime.radviz", function(periods) {
		d3.select("#radviz").select("svg").selectAll("circle")
			.style("opacity", function(d, i) { 
				var visible = periods.length == 0;
				$.each(periods, function(index, period) {
					if (d.date >= period[0] && d.date <= period[1]) 
						visible = true;
				});
				return visible ? 1 : 0;
			});	
	});		
}

function jitter(max) {
	var v = Math.pow(Math.random(), 0.3);
	if(Math.random() > 0.5)
		v = -v;
	return v*max;
}

function getMax(array_data,angle){
	var sum_x,
		sum_y,
		resultant;
	var max=1;
	for(var i=0;i<array_data.length;i++){
		sum_x=0;
		sum_y=0;
		for(var j=0;j<array_data[i].scores.length;j++){
			sum_x+=array_data[i].scores[j]*Math.cos(angle*j);
			sum_y+=array_data[i].scores[j]*Math.sin(angle*j)
		}
		resultant=getResultant(sum_x,sum_y);
		if(resultant>max){
			max=resultant;
		}
	}
	return max;
}

function getResultant(x,y){
	return Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
}