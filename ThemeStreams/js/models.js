// Here we define the models used for the application.
// Each model extends [Backbone.js' Model](http://backbonejs.org/#Model).
// * * *

// We define a `Facet`, that is based on a field in the collection of documents and a collection of these.
var Facet = Backbone.Model.extend({});
var FacetCollection = Backbone.Collection.extend({model: Facet});

// `ElasticSearchModel` contains the logics to work with [ElasticSearch](http://www.elasticsearch.org/).
var ElasticSearchModel = Backbone.Model.extend({
  // The `ElasticSearchModel` contains three default fields:
  //
  // * `facets` describing the fields in the document collection. 
  // * `query` is a dictionary in ElasticSearch's 
  //   [QueryDSL](http://www.elasticsearch.org/guide/reference/query-dsl/) 
  // * `hits`, a collection of documents.
  defaults: function() {
    return {
      "facets": new FacetCollection,
      "query": {"match_all": {}},
      "hits": new ElasticSearchHits
    }
  },

  // `initialize` gets called when a new model is created.
  initialize: function() {
    // Ensure that `this` is bound to the `ElasticSearchModel` for `newQuery`.
    _.bindAll(this, "newQuery");
    // Ensure that the default values are set.
    if(!this.get("facets")) this.set("facets", this.defaults.facets);
    if(!this.get("query")) this.set("query", this.defaults.query);
    if(!this.get("hits")) this.set("hits", this.defaults.hits);
    // Bind the change events for both query and results.
    this.bind("change:results", this.newResults, this);
    this.bind("change:query", this.newQuery, this);
  },

  // `newQuery` event is fired when the query changes.
  newQuery: function() {
    // Create a dictionary containing the query.
    var query = {"query": this.get("query")};
    // Get the results from the server and when successful set the results field.
    this.getData(
      this.get("search_url"),
      query,
      _.bind(function(data) {
        this.set("results", data);
      }, this)
    );

    // Separately get the facets. This is inefficient, but makes the app feel more responsive.
    query["facets"] = {};

    // Include each facets in the ElasticSearch query.
    this.get("facets").each(function(facet) {
      query["facets"][facet.get("field")] = {"terms": {"field": facet.get("field") }};
    });

    // Get the data with a more efficient *count* type query, storing each hit for 
    // a facet in the appropriate `Facet`.
    this.getData(
      this.get("search_url") + "&search_type=count", query,
      _.bind(function(data) {
        _.each(data["facets"], function(facet, field) {
          this.facets.where({"field": field})[0].set("hits", facet);
        });
      }, this)
    );
  },

  // `newResults` events gets fired when the results are in. Just store the hits field from this for now.
  newResults: function() { 
    this.get("hits").reset(this.get("results").hits.hits); 
  },

  // 'getData' takes care of communication with search engine, taking care of JSONifying the query.
  getData: function(url, query, callback) {
    $.ajax({
      url: url, 
      type: 'POST', 
      data: JSON.stringify(query),
      dataType : 'json',  
      processData: false, 
      success: callback, 
      error: function(xhr, message, error) {
        console.error("Error while loading data from ElasticSearch", message);
        throw(error);
      }
    });            
  }
}); 

// `PersistingCollection` is a special type of collection that ensures that all models in 
// the collection get destroyed if they are removed from the collection.
var PersistingCollection = Backbone.Collection.extend({
  initialize: function() {
    _.bindAll(this, "bindReset");
    this.bind("remove", function(model) { model.destroy(); });
    this.bind("add", this.bindReset, this);
    this.bind("reset", function() { this.each(this.bindReset); }, this);
  },
  // `bindReset` will bind the reset event to the destroy function of a model
  bindReset: function(model) {
    this.bind("reset", function() { this.destroy(); }, model); 
  }
});

// An `ElasticSearchHit` is a document in the hits that result from the query and these are 
// stored in a `PersistingCollection`.
var ElasticSearchHit  = Backbone.Model.extend({});
var ElasticSearchHits = PersistingCollection.extend({model: ElasticSearchHit});

// An `FacetHit` is a value for a view, contains just a term and a count
// and these stored in a `PersistingCollection`.
var FacetHit = Backbone.Model.extend({});
var FacetHits = PersistingCollection.extend({model: FacetHit});

// Stream to Websocket
var Stream = function(topic, url, subscriberId) {
  _.extend(this, Backbone.Events);
  var self = this;

  self.socket = new WebSocket(url);

  self.socket.onopen = function(e) {
    self.trigger('open', e);
    self.socket.send(JSON.stringify({
      "type" : "subscribe",
      "data": {
        "subscriber": subscriberId,
        "topic": topic,
      }
    }));
  }

  self.socket.onerror = function(e) {
    self.trigger('error', e);
    console.log(e);
  }

  self.socket.onmessage = function(e) {
    self.trigger('message', e);
    self.trigger('data', e.data);
    self.trigger('add_point', JSON.parse(e.data));
  }

  self.socket.onclose = function(e) {
    self.trigger('close', e);
  }
}

DataPoint = Backbone.Model.extend({
  initialize: function(options) {
    this.stream = options.stream;
    this.bin = options.bin;
    this.categories = options.categories;
    var self = this;

    this.stream.on("add_point", function(point) {
      var obj;

      try {
        obj = self.preprocessData(point.data.data.data, self.bin, self.categories);
      } catch(e) {}

      if (obj !== undefined) {
        self.set("data", obj.data);
        self.set("minDate", obj.minDate);
        self.set("maxDate", obj.maxDate);
        self.set("mean", obj.mean);
        self.set("std", obj.std);
        self.set("nTweets", obj.nTweets);

        self.trigger("dataUpdated");
      }

    });
  },

  // =================
  // helper method
  // =================

  preprocessData: function(obj, bin, categories){
    // get statistic from all data
    var dataAll = obj["all"];
    var maxDate = new Date().getTime();
    var minDate = maxDate - bin;
    var nTweets = 0;

    var mean = dataAll.reduce(function(a, b) { return a + b.value }, 0) / dataAll.length;
    var squareDiffs = dataAll.map(function(d) { d.value = Math.pow((d.value - mean),2); return d; });
    var meanSquareDiffs = dataAll.reduce(function(a, b) { return a + b.value }, 0) / dataAll.length;
    var std = Math.sqrt(meanSquareDiffs);

    // delete 'all' property
    delete obj.all;

    // pre-process data
    // only the last hour
    var resultObj = {};
    var keys = Object.keys(obj);

    for (var i=0; i < keys.length; i++) {
      if (categories.indexOf(keys[i]) == -1) {
        delete obj[keys[i]];
      } else {
        var arr = obj[keys[i]];

        var data = [];
        for (var j=0; j < arr.length; j++) {
          if (arr[j].start > minDate) {
            data.push({
              start: new Date(arr[j].start),
              end: new Date(arr[j].end),
              index: arr[j].index,
              value: arr[j].value,
              burst: false
            });

            nTweets++;
          }
        }

        // Normalize Data
        if (data.length > 0) {
          var maxValue = data.sort(function(a,b) { return b.value-a.value })[0].value;
          data = data.sort(function(a,b) { return a.start-b.start });
          data = data.map(function(d) { d.value /= maxValue; return d; });
        }

        obj[keys[i]] = data;
      }
    }

    resultObj.data = obj;
    resultObj.minDate = new Date(minDate);
    resultObj.maxDate = new Date(maxDate);
    resultObj.mean = mean;
    resultObj.std = std;
    resultObj.nTweets = nTweets;

    return resultObj;
  }
})
