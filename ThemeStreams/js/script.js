$(document).ready(function() {
  $('#static-graph').addClass('active');
  $('.datepicker').datepicker({
    dateFormat: "yy-mm-dd",
    maxDate: new Date()
  });
  $('.datepicker.start-date').val('2014-12-01');
  $('.datepicker.end-date').val($.datepicker.formatDate('yy-mm-dd', new Date()));
})
